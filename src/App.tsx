import React, { useEffect, useState } from 'react'
import BoardComponent from './components/BoardComponent'
import { Board } from './models/Board'

const App = () => {
  const [board, setBoard] = useState<Board>(new Board())

  return (
    <div>
      <BoardComponent board={board} setBoard={setBoard} />
    </div>
  )
}

export default App