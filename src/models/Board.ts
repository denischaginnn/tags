import { ICell } from "../types/ICell";
import { Cell } from "./Cell";

export class Board {
  public emptyCell: ICell = {
    value: 15,
    top: 3,
    left: 3,
  };
  private _cells: ICell[] = [];

  public get cells() {
    return this._cells;
  }

  constructor() {
   this.initBoard()
  }

  public initBoard () {
    for (let i = 0; i < 15; i++) {
        const left = i % 4;
        const top = (i - left) / 4;
        this._cells[i] = new Cell(i + 1, top, left);
      }
      this._cells.push(this.emptyCell)
  }
  public move(i: number) {
    const number = this._cells[i];
    const leftDiff = Math.abs(this.emptyCell.left - number.left);
    const topDiff = Math.abs(this.emptyCell.top - number.top);

    if (leftDiff + topDiff > 1) {
      return;
    }

    const emptyLeft = this.emptyCell.left;
    const emptyTop = this.emptyCell.top;
    this.emptyCell.left = number.left;
    this.emptyCell.top = number.top;
    number.left = emptyLeft;
    number.top = emptyTop;
  }
}
