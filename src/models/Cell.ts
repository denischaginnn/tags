import { ICell } from "./../types/ICell";

export class Cell implements ICell {
  value: number;
  top: number;
  left: number;
  constructor(value: number, top: number, left: number) {
    this.value = value;
    this.top = top;
    this.left = left;
  };
}
