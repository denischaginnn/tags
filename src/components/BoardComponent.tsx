import React, { FC } from 'react'
import { Board } from '../models/Board';

interface IPropsBoardComponent {
    board: Board;
    setBoard: (value: Board) => void;
}

const BoardComponent: FC<IPropsBoardComponent> = () => {
  return (
    <div>
      {}
    </div>
  )
}

export default BoardComponent