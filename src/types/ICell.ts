export interface ICell {
    id?: number
    value: number
    top: number
    left: number
}